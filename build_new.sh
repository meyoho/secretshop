ramls=(
"api/chen/api.raml:dist/chen.html"
"api/bane/api.raml:dist/bane.html"
"api/jakiro-raml1/api.raml:dist/jakiro-new.html"
"api/davion/api.raml:dist/davion.html"
"api/wisp/api.raml:dist/wisp.html"
"api/wisp-lords/api.raml:dist/wisp-lords.html"
"api/jakiro-furion2/api.raml:dist/jakiro-furion2.html"
"api/furion2/api.raml:dist/furion2.html"
"api/krobelus/api.raml:dist/krobelus.html"
"api/krobelus-websocket/api.raml:dist/krobelus-websocket.html"
"api/morgans/api.raml:dist/morgans.html"
"api/jakiro/api.raml:dist/index.html"
"api/jakiro/api.raml:dist/jakiro.html"
"api/druid/api.raml:dist/druid.html"
)
for r in "${ramls[@]}"; do
    IFS=":"; files=($r)
    source=${files[0]}
    dest=${files[1]}
    echo "compiling ${source} to ${dest}"
    raml2html -t static_new/index.nunjucks -i ${source} -o ${dest} || exit 1
done
