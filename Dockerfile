FROM alpine:latest

RUN apk --update add --no-cache caddy ca-certificates

RUN mkdir /static
WORKDIR /static/
CMD ["caddy"]
EXPOSE 2015

COPY Caddyfile /static/
COPY dist/ /static/
