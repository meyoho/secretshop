# What is Secret Shop

在Dota世界里SecretShop为英雄们提供神秘的魔法物品，在Alauda世界里SecretShop是开发团队的知识门户，它将所有开发团队的知识结晶汇聚起来，并以优雅的方式展示给开发者。

# How does it works

SecretShop仓库上是一个Portal项目，他为各种类型的知识文档提供简介优雅的Portal页面，并将所有的Portal页面聚合在一起并负责提供导航。

![SecretShop Architecture](./ext/SecretShop.png)

上图为SecretShop的工作原理图，SecretShop可以将本地或远程的文档项目根据其文档类型导入到相应的处理引擎中并为引擎，处理引擎用我们的自定义引擎将源文档处理成相互隔离的HTML片段，然后再将这些HTML片段使用模版引擎生成一个SPA应用的静态文件，最终将SPA的静态文件托管到静态文件服务器中。

目前SecretShop的实现还处于十分简陋的状态，仅支持RAML API格式的文档格式，路由在HTML中进行的hardcode，并使用[Aerobatic](https://www.aerobatic.com)服务作为文档构建和托管环境。本文RoadMap中描述了SecretShop接下来要走的漫漫长路。

# How to write api

目前SecretShop仅支持本地的RAML API格式的文档，因此我们目前仅可以在SecretShop项目中编写RAML API项目。    

# how to build and run on local
```
docker-compose -f docker-compose.build.yml run --rm  builder
docker-compose -f docker-compose.yml up
```

or just
```
./local.sh
```

You can visit the built API doc on localhost:2015


### 将仓库克隆到本地

```shell
git clone git@bitbucket.org:mathildetech/secretshop.git .
```

api项目全部位于`./api/`目录下，在这个目录下每个raml项目有一个单独的项目目录，例如`./api/tiny/`目录下存放的就是`jakiro`的raml项目。

### 本地修改RAML项目

建议使用[Atom](https://atom.io)编辑器结合[API Workbench](http://apiworkbench.com/)插件来编辑Raml项目，效果如下：    

![raml-editor](./ext/raml-editor.png)    

当然大家也可以使用自己喜欢的编辑器来编写Raml项目。

### 推送本地修改

修改完成之后，将修改的文件Check in到代码库，push到远程仓库后，就会触发Aerobatic进行构建和托管，构建过程的log和托管配置可以在项目中的Aerobatic设置中查看：     

![aerobatic-settings](./ext/aerobatic-settings.png) 

在Aerobatic构建完成之后即可在绑定域名或分配的域名上看到渲染好的页面了：      

![api-portal](./ext/api-portal.png)

# The meaning of Secret Shop

SecretShop存在的意义是为各个项目提供一个文档和API托管服务，每个项目本身只需按照某种文档框架的格式(如Sphinx, Raml, Jeklly等)在项目中书写文档，而无需关心这些文档的样式定义和托管这些不属于项目本身的事情，SecretShop会根据自身的配置去项目中寻找要托管的文档，然后生成并托管文档。

# 其他

## 使用 api-console 托管 api 文档
 目前只能 托管单个项目
![api-portal](./ext/apiconsole-view.png)
![api-portal](./ext/apiconsole-try.png)
### 为什么
 你可以 `try it`

### build

```
# build jakiro
# It may take a moment, just wait.
APP=jakiro docker-compose -f docker-compose.apiconsole.build.yml run  --rm builder
```
### run 

```
# run jakiro
APP=jakiro docker-compose -f docker-compose.apiconsole.yml up -d
```

# RoadMap

- [ ] 添加每种文档格式的Guideline和Best Practice
      - [ ] Raml项目的Guildeline
      - [ ] Sphinx项目的Guildeline
- [ ] 增强SecretShop的本地开发环境(SecretShop是一个前端项目，需要增添一些前段工程化的东西方便开发)
- [ ] 增加前端路由
- [ ] 支持Remote文档托管(目前仅支持托管在SecretShop仓库本身某个目录的文档，但这些文档理想情况下是应该和项目本身在同一个仓库下的)
- [ ] 增加支持文档格式的种类
      - [ ] 支持Sphinx格式
      - [ ] 支持MiddleMan格式
      - [ ] 支持Jelly格式
- [ ] 支持使用配置文件的方式来配置每个项目文档的渲染和托管(目前是硬编码的)
- [ ] 搭建自己的更佳灵活的构建和托管流程(Aerobatic限制性较强，切访问速度很慢)