
### project

1. injected project info (optional)
如果资源属于某个项目，response里会包含项目信息
```json
{ "project": { "uuid: "<uuid>", "name": "<name"> } }
```

2. project query params (optional): `project_name`


### resources actions

1.  injected resource_actions info
没有特别说明，response里都会返回`resources_actions`字段
```json
{ "resource_actions": [] }
```
