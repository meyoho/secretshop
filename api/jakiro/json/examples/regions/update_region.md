## Make NONE to KUBERNETES poc

Just list the requirement which are different from MESOS
```
{
  "version": "1.7.3",                                         # required. kubernetes version
  "platform_version": "v2/v3"                                 # optional. default(old) to v2, new to v3
  "username": "claasv1/xdzhang",
  "slave": [
    "10.1.0.14",
    "10.1.0.16"
  ],
  "is_compute": false,                                        # required. Boolean. Default: false
  "features": [
    "macvlan",                                                # required Choices [macvlan, flannel]
    "alb",
	"haproxy|nginx",
	"chronos",
	"exec",
    "flannel",
    "volume",
    "registry",
    "pipeline"
  ],
  "haproxy|nginx": {
    "public_ip": "139.219.234.191",
    "private_ip": "10.1.0.13"
  },
  "volume": {
    "volume_settings": {
      "glusterfs": {
        "path": "/data",
        "nodes": [
          "10.1.0.20",
          "10.1.0.21",
          "10.1.0.22"
        ],
        "replica": "1"
      },
      "ebs": {
        "aws_secret_key": "bbbbb",
        "aws_access_key": "aaaaa",
        "zone": "cn-north1a"                                  # required This is different from MESOS.(MESOS does not need the zone)
      }
    }
  },
  "registry": {
    "node": "10.1.0.15",
    "registry_settings": {
      "local_rootdir": "/root",
      "local_hostdir": "/home/ubuntu",
      "storage": "local",
      "name": "all_test_registry"
    }
  },
  "cidr": "10.1.0.0/24",                                      # required Spec by user in UI
  "slave_username": "ubuntu",
  "container_manager": "KUBERNETES",
  "template_type": "kubernetes_poc",
  "token": "20cefd49c0ed220b756070683163dabf34bd3c45",
  "master": {
    "public_ip": "139.219.234.191",
    "private_ip": "10.1.0.13"
  },
  "over_commit": {
    "cpu": 1,
    "memory": 2
    },
  "cni": {                                                    # required
    "type": "macvlan"                                         # required. Choices: [macvlan, flannel]. same as the element in features
  }
}



cni flannel example
"cni": {
  "network_policy": "calico",                                 # only support calico for now, set to empty to disable
  "type": "flannel",
  "flannel": {
    "type": "host-gw"                                         # Choices: ["vxlan", "host-gw", "aws-vpc", "ali-vpc"]
  }
}


cni flannel ali-vpc example                                  # Only available when region is ALI. Only support 1.7.3
"cni": {
  "type": "flannel",
  "flannel": {
    "type": "ali-vpc",
    "access_key": "xxxx",
    "secret_access_key": "xxxxxx"
  }
}
```

## Make NONE to KUBERNETES prod
```
{
  "version": "1.7.3",
  "platform_version": "v2/v3"                                 
  "is_compute": false,
  "username": "xdzhang",
  "slave": [
    "192.168.243.110"
  ],
  "features": [
    "alb",
    "haproxy",                                                                  # If want use nginx for loadbalance. change this to nginx
    "chronos",
    "exec",
    "flannel",
    "volume",
    "registry",
    "pipeline"
  ],
  "cidr": "10.1.0.0/16",
  "slave_username": "alauda",
  "container_manager": "KUBERNETES",
  "template_type": "kubernetes_prod",
  "master": [
    "192.168.243.107",
    "192.168.243.108",
    "192.168.243.109"
  ],
  "haproxy|nginx": {
    "public_ip": "47.94.100.142",
    "private_ip": "192.168.243.107",
    "nodes": [
      "192.168.243.107"
    ],
    "iaas_lb": true                                                             # Boolean. Default: false. If false. Will use keepalived to achieve HA
  },
  "cni": {
    "type": "flannel",
    "flannel": {
      "type": "ali-vpc",
      "access_key": "xxx",
      "secret_access_key": "bbb"
    }
  },
  "registry": {
    "node": [
      "192.168.243.107"
    ],
    "registry_settings": {
      "name": "fuck_ali",
      "storage": "local",
      "local_hostdir": "/root/"
    }
  },
  "volume": {
    "volume_settings": {
      "glusterfs": {
        "path": "/data",
        "replica": 1,
        "nodes": [
          "192.168.243.107",
          "192.168.243.108"
        ]
      }
    }
  },
  // new for Storageclass
  "volume": {
    "volume_settings": {
      "glusterfs": {
        "replica": 1,
        "nodes": [
          {
            "192.168.243.107": [
              "/dev/vdba",
              "/dev/vdbb"
            ]
          }
        ]
      }
    }
  },
  "controller_haproxy": {
    "floating_ip": "192.168.243.108",
    "monitor_ip": "60.205.179.33",
    "nodes": [
      "192.168.243.108"
    ],
    "iaas_lb": true
  },
  "over_commit": {
    "cpu": 3,
    "memory": 3
  },
  "root_token": "d85dd771aad1b0df389213cc529243a24c17af43"
}

```

## Make NONE to MESOS poc
```
{
  "username": "claasv1/xdzhang",                        # required
  "slave": [                                            # required
    "10.1.0.14",
    "10.1.0.16"
  ],
  "features": [                                         # required
    "internal-haproxy",
    "haproxy",
    "exec",
    "registry",
    "volume",
    "glusterfs",
    "alb"
  ],
  "is_compute": false,                                  # required, Boolean. Default: false
  "volume": {                                           # Optional Must spec when features contain volume
    "volume_settings": {                                # required
      "glusterfs": {                                    # Optional Must spec when features contain glusterfs
        "path": "/data",
        "nodes": [
          "10.1.0.20",
          "10.1.0.21",
          "10.1.0.22"
        ],
        "replica": "1"
      },
      "ebs": {                    # Optional
        "aws_secret_key": "bbbbb",
        "aws_access_key": "aaaaa"
      }
    }
  },
  "registry": {                                        # Optional Must spec when features contain registry
    "node": "10.1.0.15",
    "registry_settings": {
      "local_rootdir": "/root",
      "local_hostdir": "/home/ubuntu",
      "storage": "local",
      "name": "all_test_registry"
    }
  },
  "over_commit": {
    "cpu": 1,
    "memory": 2
    },
  "slave_username": "ubuntu",                           #required
  "internal-haproxy": "10.1.0.17",                      # Optional Must spec when features contain internal-haproxy
  "slave_with_tag": [
    "10.1.0.16"
  ],
  "container_manager": "MESOS",                         # required
  "template_type": "poc",                               # required
  "token": "20cefd49c0ed220b756070683163dabf34bd3c45",  # required
  "master": {                                           # required
    "public_ip": "139.219.234.191",
    "private_ip": "10.1.0.13"
  }
}
```

## Make NONE to MESOS PROD

Basiclly same as poc. the difference is the key of master and the template_type

```
{
  "username": "claasv1/xdzhang",
  "haproxy": {
    "public_ip": "139.219.234.191",                     # Monitor ip
    "private_ip": "10.1.0.13",                          # floatting ip
    "nodes": ["172.31.1.1", "172.31.1.2"]               # nodes
  },
  "slave_username": "ubuntu",
  "internal-haproxy": "10.1.0.17",
  "slave": [
    "10.1.0.14",
    "10.1.0.16"
  ],
  "is_compute": true,
  "features": [
    "internal-haproxy",
    "haproxy",
    "exec",
    "registry",
    "volume",
    "glusterfs",
    "alb",
    "registry",
    "pipeline",
    "alb"
  ],
  "container_manager": "MESOS",
  "slave_with_tag": [
    "10.1.0.16"
  ],
  "template_type": "prod",
  "token": "20cefd49c0ed220b756070683163dabf34bd3c45",
  "master": [
    "10.1.0.10",
    "10.1.0.11",
    "10.1.0.12"
  ],
  "registry": {
    "node": "10.1.0.15",
    "registry_settings": {
      "local_rootdir": "/root",
      "local_hostdir": "/home/ubuntu",
      "storage": "local",
      "name": "all_test_registry"
    }
  },
  "over_commit": {
    "cpu": 1,
    "memory": 2
    },
  "volume": {
    "volume_settings": {
      "glusterfs": {
        "path": "/data",
        "nodes": [
          "10.1.0.20",
          "10.1.0.21",
          "10.1.0.22"
        ],
        "replica": "1"
      },
      "ebs": {
        "aws_secret_key": "bbbbb",
        "aws_access_key": "aaaaa"
      }
    }
  }
}
```

## Make NONE to SWARM

Just list the requirement which are different from MESOS
```
{
  "username": "claasv1/xdzhang",
  "slave": [
    "10.1.0.14",
    "10.1.0.16"
  ],
  "features": [  
    "haproxy",
    "registry"
  ],
  "registry": {
    "node": "10.1.0.15",
    "registry_settings": {
      "local_rootdir": "/root",
      "local_hostdir": "/home/ubuntu",
      "storage": "local",
      "name": "all_test_registry"
    }
  },
  "haproxy": {
    "public_ip": "139.219.234.191",
    "private_ip": "10.1.0.13"
  },
  "slave_username": "ubuntu",
  "slave_with_tag": [
    "10.1.0.16"
  ],
  "container_manager": "SWARM",
  "template_type": "poc",               # required Only support poc
  "token": "20cefd49c0ed220b756070683163dabf34bd3c45",
  "master": {
    "public_ip": "139.219.234.191",
    "private_ip": "10.1.0.13"
  }
}
```
