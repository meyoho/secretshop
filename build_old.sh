ramls=(
"api/balanar/api.raml:dist/balanar.html"
"api/jakiro2/api.raml:dist/jakiro2.html"
"api/jakiro3/api.raml:dist/jakiro3.html"
"api/furion/api.raml:dist/furion.html"
"api/lucifer/api.raml:dist/lucifer.html"
"api/mirana/api.raml:dist/mirana.html"
"api/mirana2/api.raml:dist/mirana2.html"
"api/puck/api.raml:dist/puck.html"
"api/razzil/api.raml:dist/razzil.html"
"api/sven/api.raml:dist/sven.html"
"api/sven2/api.raml:dist/sven2.html"
"api/tiny/api.raml:dist/tiny.html"
"api/tiny-proxy/api.raml:dist/tiny-proxy.html"
"api/tiny2/api.raml:dist/tiny2.html"
"api/enigma/api.raml:dist/enigma.html"
"api/windranger/api.raml:dist/windranger.html"
"api/oracle/api.raml:dist/oracle.html"
"api/chen2/api.raml:dist/chen2.html"
"api/heimdall/api.raml:dist/heimdall.html"
"api/weaver/api.raml:dist/weaver.html"
"api/architect/api.raml:dist/architect.html"
"api/medusa/api.raml:dist/medusa.html"
"api/darchrow/api.raml:dist/darchrow.html"
"api/tresdin/api.raml:dist/tresdin.html"
"api/observer/api.raml:dist/observer.html"
"api/observer2/api.raml:dist/observer2.html"
"api/thrall/api.raml:dist/thrall.html"
"api/visage/api.raml:dist/visage.html"
"api/phoenix/api.raml:dist/phoenix.html"
"api/ddserver/api.raml:dist/ddserver.html"
"api/riki/api.raml:dist/riki.html"
"api/lightkeeper/api.raml:dist/lightkeeper.html"
"api/jenkins-api-plugin/api.raml:dist/jenkins-api-plugin.html"
"api/chen2/api.raml:dist/chen2.html"
)
for r in "${ramls[@]}"; do
    IFS=":"; files=($r)
    source=${files[0]}
    dest=${files[1]}
    echo "compiling ${source} to ${dest}"
    raml2html -t static/template.nunjucks -i ${source} -o ${dest} || exit 1
done

